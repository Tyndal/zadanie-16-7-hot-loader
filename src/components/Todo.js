import React from "react";
import style from "./Todo.css";

const Todo = props => (
  <li className={style.Todo}>
    <p>{props.todo}</p>
    <button onClick={event => props.remove(props.id)}>x Usuń</button>
  </li>
);

export default Todo;
