import React from "react";
import style from "./TodoForm.css"

const TodoForm = props => (
  <form
    onSubmit={event => {
      event.preventDefault();
      props.addTodo(event.target.querySelector("input").value);
    }} className={style.TodoForm}
  >
    <input />
    <button>Dodaj</button>
  </form>
);

export default TodoForm;
